
#include<stdio.h>

float read_number();
float compute_sum(float,float);
void display_sum(float,float,float);

int main()
{
     float a=read_number();
     float b=read_number();
     float add=compute_sum(a,b);
     display_sum(a,b,add);
     return 0;
}

float read_number()
{
     float x;
     printf("Enter a number :\n");
     scanf("%f",&x);
     return x; 
}

float compute_sum(float c,float d)
{
      float s=c+d;
      return s;
}

void display_sum(float p,float q,float r)
{
    printf("Sum of %f and %f is :%f\n",p,q,r);
}
