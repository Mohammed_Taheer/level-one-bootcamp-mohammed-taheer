//WAP to find the distance between two point using 4 functions.

#include<stdio.h>
#include<math.h>

float input_points(char,int);
float calculate_distance(float,float,float,float);
void distance_output(float,float,float,float,float);

int main()
{
    float x1=input_points('x',1);
    float y1=input_points('y',1);
    float x2=input_points('x',2);
    float y2=input_points('y',2);
    float distance=calculate_distance(x1,x2,y1,y2);
    distance_output(distance,x1,x2,y1,y2);
    return 0;
}

float input_points(char v,int a)
{
    float point;
    printf("Enter the value for %c%d:\n",v,a);
    scanf("%f",&point);
    return point;
}

float calculate_distance(float x1,float x2,float y1,float y2)
{
    float dis=sqrt((x2-x1)*(x2-x1)+(y2-y1)*(y2-y1));
    return dis;
}

void distance_output(float dis,float x1,float x2,float y1,float y2)
{
    printf("The distance between (x1,y1)=(%f,%f) and (x2,y2)=(%f,%f) is:%f\n",x1,y1,x2,y2,dis);
}
