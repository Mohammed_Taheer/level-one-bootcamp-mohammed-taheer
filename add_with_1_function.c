//Write a program to add two user input numbers using one function.

#include<stdio.h>
int main()
{
    float a,b;
    printf("Input:\n");
    printf("Enter first number:\n");
    scanf("%f",&a);
    printf("Enter second number:\n");
    scanf("%f",&b);
    printf("Output:\n");
    printf("Sum of %f and %f is:%f\n",a,b,a+b);
    return 0;
}

