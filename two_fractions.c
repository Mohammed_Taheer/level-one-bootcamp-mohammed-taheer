//WAP to find the sum of two fractions.

#include<stdio.h>

struct fract
{
     int nume,deno;
};

struct fract input_fract(struct fract,int);
struct fract sum_fract(struct fract,struct fract);
int gcd_value(struct fract);
void display_fract(struct fract,struct fract,struct fract);

int main()
{
     struct fract f1,f2,result;
     f1=input_fract(f1,1);
     f2=input_fract(f2,2);
     result=sum_fract(f1,f2);
     display_fract(f1,f2,result);
     return 0;
}

struct fract input_fract(struct fract f,int x)
{
     printf("Enter the numerator and denominator of the %d st/nd number respectively:\n",x);
     scanf("%d%d",&f.nume,&f.deno);
     return f;
}

struct fract sum_fract(struct fract f1,struct fract f2)
{
   struct fract sum={(f1.nume*f2.deno)+(f2.nume*f1.deno),f1.deno*f2.deno};
   int gcd=gcd_value(sum);
   struct fract sum_value= {(sum.nume/gcd),(sum.deno/gcd)};
   return sum_value;
     
}

int gcd_value(struct fract result)
{
    int i,j;
     for(i=1;i<=result.nume && i<=result.deno;i++)
     {
         if(result.nume%i==0 && result.deno%i==0)
         {
             j=i;
         }
     }
     return j;
}

void display_fract(struct fract f1,struct fract f2,struct fract result)
{
     printf("The sum of %d / %d and %d / %d is:%d / %d\n",f1.nume,f1.deno,f2.nume,f2.deno,result.nume,result.deno);

}
