//Write a program to find the sum of n different numbers using 4 functions

#include<stdio.h>

int input_num();
void input_values(int,float []);
float sum_values(int,float []);
void display_sum(int,float [],float);

int main()
{
     int num;
     float sum;
     num=input_num();
     float value[num];
     input_values(num,value);
     sum=sum_values(num,value);
     display_sum(num,value,sum);
     return 0;
    
}

int input_num()
{
      int num;
     printf("Enter how many different numbers :\n");
     scanf("%d",&num);
     return num;
}

void input_values(int num,float value[num])
{
      int i;
      for (i=0;i<num;i++)
    {
       printf("Enter number %d :\n",i+1);
       scanf("%f",&value[i]);
    }
}

float sum_values(int num,float value[num])
{
      int i;
      float sum=0;
      for (i=0;i<num;i++)
      {
         sum=sum+value[i];
      }
      return sum;
}

void display_sum(int num,float value[num],float sum)
{      
    int i;
    printf("Sum of %d different numbers =\n",num);
    for(i=0;i<num;i++)
        printf(" %f, ",value[i]);
    printf("is %f",sum);
}