//Write a program to find the volume of a tromboloid using one function

#include<stdio.h>

int main()
{
     float h,d,b,vol;
     printf("Input:\n");
     printf("Enter the value of height,depth and breadth respectively:\n");
     scanf("%f%f%f",&h,&d,&b);
     vol= ((h*d*b)+(d/b))/3;
     printf("Output:\n");
     printf("Volume of tromboloid is:%f\n",vol);
     return 0;
}
