//WAP to find the sum of n fractions.

#include<stdio.h>

struct fraction
{
	int num;
	int den;
};

int input_no();
void input_fract(int,struct fraction fract[]);
struct fraction input_sum(int ,struct fraction fract[]);
int lcm_den(int,struct fraction fract[]);
int gcd(int,int);
void display_sum(int,struct fraction fract[],struct fraction );

int main()
{
	int n=input_no();
	struct fraction fract[n],sum;
	input_fract(n,fract);
	sum=input_sum(n,fract);
	display_sum(n,fract,sum);
	return 0;
}

int input_no()
{
	int n;
	printf("Enter number of fractions:\n");
	scanf("%d",&n);
	return n;
}

void input_fract(int n,struct fraction fract[n])
{
	for(int i=0;i<n;i++)
	{
		printf("Enter numerator value of %d:\n",i+1);
		scanf("%d",&fract[i].num);
		printf("Enter denominator value of %d:\n",i+1);
		scanf("%d",&fract[i].den);
	
	}
}

struct fraction input_sum(int n,struct fraction fract[n])
{
	struct fraction sum;
	sum.num=0;
	sum.den=lcm_den(n,fract);
	for(int i=0;i<n;i++)
	{
	    sum.num=sum.num+(fract[i].num)*(sum.den/fract[i].den);
	}
	
	int GCD=gcd(sum.num,sum.den);
	
	sum.num=sum.num/GCD;
	sum.den=sum.den/GCD;
	
	return sum;
}

int lcm_den(int n,struct fraction fract[n] )
{
    int den=fract[0].den;
    for(int i=1;i<n;i++)
    {
        den=(((den*fract[i].den))/(gcd(fract[i].den,den)));
    }
    return den;
}

int gcd(int x,int y)
{
    if (y == 0) {
        return x;
    }
 
    return gcd(y, x % y);
}

void display_sum(int n,struct fraction fract[n],struct fraction sum)
{
	printf("sum of %d different fractions ; \n",n);
	for(int i=0;i<n;i++)
	{
	    printf(" %d/%d ",fract[i].num,fract[i].den);
	}
	printf("is %d / %d \n",sum.num,sum.den);
}