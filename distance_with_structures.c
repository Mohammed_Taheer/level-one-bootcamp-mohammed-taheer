//WAP to find the distance between two points using structures and 4 functions.

#include<stdio.h>
#include<math.h>
#include<stdlib.h>

struct point
{
    float x,y;
};

float input_points(char,int);
float calculate_distance(struct point,struct point);
void distance_output(float,struct point,struct point);

int main()
{
    struct point p1,p2;
    p1.x=input_points('x',1);
    p1.y=input_points('y',1);
    p2.x=input_points('x',2);
    p2.y=input_points('y',2);
    float distance=calculate_distance(p1,p2);
    distance_output(distance,p1,p2);
    return 0;
}

float input_points(char v,int a)
{
    float point;
    printf("Enter the value for %c%d:\n",v,a);
    scanf("%f",&point);
    return point;
}

float calculate_distance(struct point p1,struct point p2)
{
    float dis=sqrt((p2.x-p1.x)*(p2.x-p1.x)+(p2.y-p1.y)*(p2.y-p1.y));
    return dis;
}

void distance_output(float dis,struct point p1,struct point p2)
{
    printf("The distance between (x1,y1)=(%f,%f) and (x2,y2)=(%f,%f) is:%f\n",p1.x,p1.y,p2.x,p2.y,dis);
}
