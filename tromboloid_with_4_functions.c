#include<stdio.h>

float values(char);
float volume(float,float,float);
void display_volume(float);

int main()
{
    float x=values('H');
    float y=values('B');
    float z=values('D');
    float p=volume(x,y,z);
    display_volume(p);
    return 0;
}

float values(char var)
{
    printf("Enter the %c:\n",var);
    float a;
    scanf("%f",&a);
    return a;
}

float volume(float i, float j,float k)
{
    
    float f =((i*j*k)+(k/j))/3;
    return f;
}

void display_volume(float q)
{
    printf("Output:\nVolume of tromboloid is:%f\n",q);
}